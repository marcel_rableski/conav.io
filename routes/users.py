# coding=utf-8
import datetime
import logging
import sys
from os import getenv, path, system

import pandas as pd
from flask import current_app, jsonify, request

import pyodbc
from services import Translations, Database, Validation

db = Database()
i18n = Translations(current_app.config['LANG'])
validator = Validation(current_app.config['LANG'])


@current_app.route('/users')
@current_app.route('/user/<username>')
def getUsers(username=None):
    # set vars
    usersDB = []
    users = []

    json = {
        'status': 'cancel',
        'users': usersDB
    }

    try:
        # connect and fetch data
        selects = [
            'U.[User Name] AS UserName',
            'U.[Full Name] AS UserFullname',
            'Pe.[Company] AS UserPersonalization',
            "STUFF((SELECT ';' + [Role ID] FROM dbo.[{0}$ES "
            "Login Access Control] WHERE [Login User ID] = U.[User Name] "
            "FOR XML PATH ('')), 1, 1, '') AS UserAccess".format(
                db.getTables().get('easy')
            )
        ]

        joins = []

        for index, table in db.getTables().items():
            selects.append(
                'Pr{0}.[User ID] AS UserPreference{0}'.format(index.title())
            )

            selects.append(
                'Sa{0}.[Code] AS UserSales{0}'.format(index.title())
            )

            selects.append(
                'Se{0}.[User ID] AS UserSetup{0}'.format(index.title())
            )

            selects.append(
                '(SELECT COUNT(Ur.[User Roles]) from dbo.[{table}$NCFB '
                'Map User Roles] AS Ur WHERE U.[User Name] LIKE \'%\' + Ur.'
                '[Salesperson Code]) AS UserRole{title}'.format(
                    table=table,
                    title=index.title()
                )
            )

            joins.append(
                "LEFT JOIN dbo.[{table}$NCFB User Preference] as Pr{index} "
                "ON Pr{index}.[User ID] = U.[User Name] "
                "LEFT JOIN dbo.[{table}$User Setup] as Se{index} "
                "ON Se{index}.[User ID] = U.[User Name] "
                "LEFT JOIN dbo.[{table}$Salesperson_Purchaser] as Sa{index} "
                "ON U.[User Name] LIKE '%' + Sa{index}.[Code]".format(
                    table=table,
                    index=index.title()
                )
            )

        # filter select when username is active
        where = ''

        if username:
            where = "WHERE U.[User Name] = 'ELBE\{0}'".format(username.upper())

        query = 'SELECT {selects} FROM dbo.[User] as U LEFT JOIN ' \
            'dbo.[User Personalization] AS Pe ON ' \
            'U.[User Security ID] = Pe.[User SID] {joins} {where}'.format(
                selects=', '.join(selects),
                joins=' '.join(joins),
                where=where
            )

        cursor = db.getCursor()
        if cursor:
            cursor.execute(query)
            columns = [column[0] for column in cursor.description]
            usersDB = [dict(zip(columns, row)) for row in cursor.fetchall()]

            for user in usersDB:
                UserPersonalization = user.get('UserPersonalization') or ''
                UserAccess = user.get('UserAccess') or ''

                UserPreferenceTables = [
                    v
                    for k, v in db.getTables().items()
                    if user.get('UserPreference{0}'.format(
                                k.title())) is not None
                ]

                UserSalesTables = [
                    v
                    for k, v in db.getTables().items()
                    if user.get('UserSales{0}'.format(
                                k.title())) is not None
                ]
                UserSetupTables = [
                    v
                    for k, v in db.getTables().items()
                    if user.get('UserSetup{0}'.format(
                                k.title())) is not None
                ]
                UserRoleTables = [
                    v
                    for k, v in db.getTables().items()
                    if user.get('UserRole{0}'.format(
                                k.title())) > 0
                ]

                countTables = len(db.getTables())

                userObj = {
                    'UserName': user.get('UserName', ''),
                    'UserFullname': user.get('UserFullname', ''),
                    'valid': (
                        len(UserPersonalization) > 0 and
                        len(UserPreferenceTables) == countTables and
                        len(UserSalesTables) == countTables and
                        len(UserSetupTables) == countTables and
                        len(UserRoleTables) == countTables and
                        len(UserAccess) > 0
                    )
                }

                if username:
                    userObj.update({
                        'UserPersonalization': {
                            'valid': UserPersonalization
                        },
                        'UserPreference': {
                            'valid': len(UserPreferenceTables) == countTables,
                            'tables': UserPreferenceTables
                        },
                        'UserSales': {
                            'valid': len(UserSalesTables) == countTables,
                            'tables': UserSalesTables
                        },
                        'UserSetup': {
                            'valid': len(UserSetupTables) == countTables,
                            'tables': UserSetupTables
                        },
                        'UserRole': {
                            'valid': len(UserRoleTables) == countTables,
                            'tables': UserRoleTables
                        },
                        'UserAccess': {
                            'valid': UserAccess if UserAccess else ''
                        }
                    })

                users.append(userObj)

            json.update({
                'status': 'success',
                'users': users
            })

        if getenv('DEBUG'):
            json.update({
                'debug': {
                    'query': query
                }
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        res.status_code = 400

        return res


@current_app.route('/user/personalization', methods=['POST'])
def updateUserPersonalization():
    try:
        data = request.get_json(silent=True) or {}
        validation = validator.validateUser(data, ['company', 'username'])

        json = {
            'status': 'cancel',
            'updated_rows': 0
        }

        if validation.get('valid'):
            profile = data.get('profile') or 'FINANCESALESORDER'
            company = data.get('company')
            username = data.get('username')

            columns = [
                '[Profile ID]',
                '[Language ID]',
                '[Company]'
            ]

            values = [
                profile,
                1031,
                company
            ]

            updates = [
                "{x} = '{v}'".format(x=x, v=values[columns.index(x)])
                for x in columns
            ]

            query = "UPDATE dbo.[User Personalization] SET {columns} WHERE " \
                "[User SID] = (SELECT [User Security ID] " \
                "FROM dbo.[User] WHERE [User Name] = " \
                "'ELBE\{username}')".format(
                    columns=', '.join(updates),
                    username=username.upper()
                )

            cursor = db.getCursor()
            if cursor:
                cursor.execute(query)
                cursor.commit()

                json.update({
                    'status': 'success',
                    'updated_rows': cursor.rowcount
                })

            if getenv('DEBUG'):
                json.update({
                    'debug': {
                        'query': query
                    }
                })

        else:
            json.update({
                'status': 'error',
                'message': ','.join(validation.get('errors'))
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        res.status_code = 400

        return res


@current_app.route('/user/preference', methods=['POST'])
def postUserPreference():
    try:
        data = request.get_json(silent=True) or {}
        validation = validator.validateUser(data, ['username'])

        json = {
            'status': 'cancel',
            'updated_rows': 0
        }

        if validation.get('valid'):
            username = data.get('username')
            updateTables = data.get('tables') or db.getTables().keys()

            columns = [
                '[User ID]',
                '[Language Code]',
                '[Country_Region Code]',
                '[Sign 1 Code]',
                '[Sign 2 Code]',
                '[Exp_ Handling Batch Name]',
                '[Refinancing Template Name]',
                '[Business Unit Code]',
                '[Int_ Adjmt_ Batch Name]'
            ]
            if getenv('MSSQL_SOURCE') == 'live':
                columns.append('[Partner No_]')
            else:
                columns.append('[Partner Code]')

            values = [
                'ELBE\{username}'.format(username=username.upper()),
                'DEU',
                'DE',
                'KEMPF',
                'KEMPF',
                '',
                '',
                '100',
                '',
                'STANDARD'
            ]

            query = []

            for index, table in db.getTables().items():
                if index in updateTables:
                    if 'EASY' in table:
                        updates = [
                            "{x} = '{v}'".format(
                                x=x, v=values[columns.index(x)]
                            )
                            for x in columns
                            if 'User ID' not in x
                        ]

                        query.append(
                            "UPDATE dbo.[{table}$NCFB User Preference] SET "
                            "{columns} WHERE {userCol} = '{userVal}'".format(
                                table=table,
                                columns=', '.join(updates),
                                userCol=columns[0],
                                userVal=values[0]
                            )
                        )
                    else:
                        query.append(
                            "INSERT INTO dbo.[{table}$NCFB User Preference] "
                            "({columns}) VALUES ('{values}')".format(
                                columns=', '.join(columns),
                                values="', '".join(values),
                                table=table
                            ))

            cursor = db.getCursor()
            if cursor:
                cursor.execute(";".join(query))
                cursor.commit()

                json.update({
                    'status': 'success',
                    'updated_rows': cursor.rowcount
                })

            if getenv('DEBUG'):
                json.update({
                    'debug': {
                        'query': query
                    }
                })

        if not validation.get('valid'):
            json.update({
                'status': 'error',
                'message': ','.join(validation.get('errors'))
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        res.status_code = 400

        return res


@current_app.route('/user/sales', methods=['POST'])
def postUserSales():
    try:
        data = request.get_json(silent=True) or {}
        validation = validator.validateUser(
            data, ['username', 'fullname', 'salutation', 'email', 'phone'])

        json = {
            'status': 'cancel',
            'updated_rows': 0
        }

        if validation.get('valid'):
            username = data.get('username')
            fullname = data.get('fullname')
            salutation = data.get('salutation')
            email = data.get('email') or ''
            phone = data.get('phone') or ''
            updateTables = data.get('tables') or db.getTables().keys()

            columns = [
                'Code',
                'Name',
                '[Commission _]',
                '[Global Dimension 1 Code]',
                '[Global Dimension 2 Code]',
                '[E-Mail]',
                '[Phone No_]',
                '[Job Title]',
                '[Search E-Mail]',
                '[E-Mail 2]',
                'Staff',
                '[Authorization Password]',
                '[Self Authorization Possible]',
                '[Company Location Code]',
                '[Salutation Code]',
                '[Fax No_]',
                '[Mobile Phone No_]',
                '[Language Code]',
                'Inactive',
                '[Date Inactive]',
                '[Cont_ No_ Sales Comm_ Remittee]',
                '[Absenteeism From]',
                '[Absenteeism To]',
                '[Receive Interaction Mail]'
            ]

            values = [
                username.upper(),
                fullname,
                '0',
                '',
                '',
                email,
                phone,
                '',
                email.upper(),
                '',
                '1',
                '',
                '1',
                '001',
                salutation.upper(),
                '',
                '',
                'DEU',
                '0',
                '1753-01-01 00:00:00.000',
                '',
                '1753-01-01 00:00:00.000',
                '1753-01-01 00:00:00.000',
                '1'
            ]

            query = []

            for index, table in db.getTables().items():
                if index in updateTables:
                    query.append(
                        "INSERT INTO dbo.[{table}$Salesperson_Purchaser] "
                        "({columns}) VALUES ('{values}')".format(
                            columns=", ".join(columns),
                            values="', '".join(values),
                            table=table
                        ))

            cursor = db.getCursor()
            if cursor:
                cursor.execute(";".join(query))
                cursor.commit()

                json.update({
                    'status': 'success',
                    'updated_rows': cursor.rowcount
                })

            if getenv('DEBUG'):
                json.update({
                    'debug': {
                        'query': query
                    }
                })

        if not validation.get('valid'):
            json.update({
                'status': 'error',
                'message': ','.join(validation.get('errors'))
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        res.status_code = 400

        return res


@current_app.route('/user/setup', methods=['POST'])
def postUserSetup():
    try:
        data = request.get_json(silent=True) or {}
        validation = validator.validateUser(data, ['username'])

        json = {
            'status': 'cancel',
            'updated_rows': 0
        }

        if validation.get('valid'):
            username = data.get('username')
            updateTables = data.get('tables') or db.getTables().keys()

            columns = [
                '[User ID]',
                '[Allow Posting From]',
                '[Allow Posting To]',
                '[Register Time]',
                '[Salespers__Purch_ Code]',
                '[Approver ID]',
                '[Sales Amount Approval Limit]',
                '[Purchase Amount Approval Limit]',
                '[Unlimited Sales Approval]',
                '[Unlimited Purchase Approval]',
                'Substitute',
                '[E-Mail]',
                '[Request Amount Approval Limit]',
                '[Unlimited Request Approval]',
                '[Time Sheet Admin_]',
                '[Allow FA Posting From]',
                '[Allow FA Posting To]',
                '[Sales Resp_ Ctr_ Filter]',
                '[Purchase Resp_ Ctr_ Filter]',
                '[Service Resp_ Ctr_ Filter]',
                'Developer',
                '[Developer Set By]',
                '[Developer Set Date]',
                '[Developer Set Time]',
                '[Cue Role Center Type]',
                '[Cont_ Analysis Template 1]',
                '[Cont_ Analysis Period Length 1]',
                '[Cont_ Analysis Template 2]',
                '[Cont_ Analysis Period Length 2]',
                '[Cont_ Analysis Template 3]',
                '[Cont_ Analysis Period Length 3]',
                '[Cont_ Analysis Template 4]',
                '[Cont_ Analysis Period Length 4]',
                '[Term_ (Borr_) Reference Date]'
            ]

            values = [
                'ELBE\{username}'.format(username=username.upper()),
                '1753-01-01 00:00:00.000',
                '1753-01-01 00:00:00.000',
                '0',
                username.upper(),
                '',
                '0',
                '0',
                '0',
                '0',
                '',
                '',
                '0',
                '0',
                '0',
                '1753-01-01 00:00:00.000',
                '1753-01-01 00:00:00.000',
                '',
                '',
                '',
                '0',
                '',
                '1753-01-01 00:00:00.000',
                '1753-01-01 00:00:00.000',
                '0',
                '',
                '0',
                '',
                '0',
                '',
                '0',
                '',
                '0',
                '1753-01-01 00:00:00.000'
            ]

            query = []

            for index, table in db.getTables().items():
                if index in updateTables:
                    if 'EASY' in table:
                        updates = [
                            "{x} = '{v}'".format(
                                x=x, v=values[columns.index(x)])
                            for x in columns
                            if 'Salespers__Purch_ Code' in x
                        ]

                        query.append(
                            "UPDATE dbo.[{table}$User Setup] SET "
                            "{columns} WHERE {userCol} = '{userVal}'".format(
                                table=table,
                                columns=', '.join(updates),
                                userCol=columns[0],
                                userVal=values[0]
                            )
                        )
                    else:
                        query.append(
                            "INSERT INTO dbo.[{table}$User Setup] "
                            "({columns}) VALUES ('{values}')".format(
                                columns=', '.join(columns),
                                values="', '".join(values),
                                table=table
                            ))

            cursor = db.getCursor()
            if cursor:
                cursor.execute(";".join(query))
                cursor.commit()

                json.update({
                    'status': 'success',
                    'updated_rows': cursor.rowcount
                })

            if getenv('DEBUG'):
                json.update({
                    'debug': {
                        'query': query
                    }
                })

        if not validation.get('valid'):
            json.update({
                'status': 'error',
                'message': ','.join(validation.get('errors'))
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })
        res.status_code = 400

        return res


@current_app.route('/user/roles', methods=['POST'])
def postUserRoles():
    try:
        data = request.get_json(silent=True) or {}

        validation = validator.validateUser(
            data, ['username'])

        json = {
            'status': 'cancel',
            'updated_rows': 0
        }

        if validation.get('valid'):
            username = data.get('username')
            updateTables = data.get('tables') or db.getTables().keys()

            columns = [
                '[Salesperson Code]',
                '[Represent Salesperson Code]',
                '[User Roles]'
            ]

            values = [
                username.upper(),
                ''
            ]

            query = []

            roles = ['ALL', 'ACTIVATION', 'APPROVAL',
                     'APPROVAL2', 'HANDLING', 'SALES', 'SALESMANAG']

            for index, table in db.getTables().items():
                if index in updateTables:
                    for role in roles:
                        copy = values.copy()
                        copy.append(role)
                        query.append(
                            "INSERT INTO dbo.[{table}$NCFB Map User Roles] "
                            "({columns}) VALUES ('{values}')".format(
                                columns=", ".join(columns),
                                values="', '".join(copy),
                                table=table
                            ))

            cursor = db.getCursor()

            if cursor:
                cursor.execute(";".join(query))
                cursor.commit()

                json.update({
                    'status': 'success',
                    'updated_rows': cursor.rowcount
                })

            if getenv('DEBUG'):
                json.update({
                    'debug': {
                        'query': query
                    }
                })

        if not validation.get('valid'):
            json.update({
                'status': 'error',
                'message': ','.join(validation.get('errors'))
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        res = jsonify({
            'status': 'error',
            'message': '{0}'.format(error)
        })

        res.status_code = 400

        return res


@current_app.route('/user/access', methods=['POST'])
def postUserAccess():
    try:
        data = request.get_json(silent=True) or {}
        validation = validator.validateUser(
            data, ['username'])

        json = {
            'status': 'cancel',
            'updated_rows': 0
        }

        if validation.get('valid'):
            username = data.get('username')
            date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.000')
            updateTable = db.getTables('easy')

            columns = [
                '[Login Type]',
                '[Login User ID]',
                '[Role Type]',
                '[Role ID]',
                '[Company Group ID]',
                '[Created By User]',
                '[Created Date Time]',
                '[No Permissions]',
                '[Expiry Date]'
            ]

            values = [
                '2',
                'ELBE\{username}'.format(username=username.upper()),
                '0',
                'SUPER',
                'MG_ALL',
                getenv('MSSQL_USER').upper(),
                date,
                '0',
                '1753-01-01 00:00:00.000'
            ]

            query = "INSERT INTO dbo.[{table}$ES Login Access Control] " \
                "({columns}) VALUES ('{values}')".format(
                    columns=", ".join(columns),
                    values="', '".join(values),
                    table=updateTable
                )

            cursor = db.getCursor()
            if cursor:
                cursor.execute(query)
                cursor.commit()

                json.update({
                    'status': 'success',
                    'updated_rows': cursor.rowcount
                })

            if getenv('DEBUG'):
                json.update({
                    'debug': {
                        'query': query
                    }
                })

        if not validation.get('valid'):
            json.update({
                'status': 'error',
                'message': ','.join(validation.get('errors'))
            })

        return jsonify(json)

    except Exception as error:
        logging.error(error)

        json = {
            'status': 'error',
            'message': '{0}'.format(error)
        }

        if getenv('DEBUG'):
            json.update({
                'debug': {
                    'query': query
                }
            })

        res = jsonify(json)
        res.status_code = 400

        return res
