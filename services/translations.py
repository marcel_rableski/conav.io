class Translations():
    def __init__(self, lang):
        self.__lang = lang
        self.__data = data

    def get(self, key=None):
        if key and self.__lang:
            return self.__data.get(self.__lang).get(key) or key
        else:
            return key


data = {
    'de': {
        'invalid entry point': 'Ungültiger API Endpunkt',
        'data required': 'Daten fehlen',
        'no valid company': 'Firma überprüfen',
        'no valid username': 'Username überprüfen'
    },
    'en': {
        'invalid entry point': 'invalid entry point test'
    }
}
