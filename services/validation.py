import logging
from services import Translations, Database

db = Database()


class Validation:
    def __init__(self, lang):
        self.__lang = lang

    def validateUser(self, data=None, fields=None):
        errors = []
        valid = True

        i18n = Translations(self.__lang)

        try:
            if data is None or len(data) == 0:
                errors.append(i18n.get('no valid data'))

            # tables = data.get('tables').replace(' ', '').split(',') or []
            # intersect = set(tables).difference(list(db.getTables().keys()))
            # if intersect:
            #     errors.append(i18n.get('no valid tables'))

            company = data.get('company')
            if (
                'company' in fields and
                company not in db.getTables().values()
            ):
                errors.append(i18n.get('no valid company'))

            username = data.get('username')
            if 'username' in fields and not username:
                errors.append(i18n.get('no valid username'))

            fullname = data.get('fullname')
            if 'fullname' in fields and not fullname:
                errors.append(i18n.get('no valid fullname'))

            salutation = data.get('salutation')
            if 'salutation' in fields and salutation not in ['HERR', 'FRAU']:
                errors.append(i18n.get('no valid salutation'))

            email = data.get('email') or '@'
            if ('email' in fields and '@' not in email):
                errors.append(i18n.get('no valid email'))

            phone = data.get('phone') or '+'
            if ('phone' in fields and '+' not in phone):
                errors.append(i18n.get('no valid phone'))

            if len(errors) > 0:
                valid = False

            return {
                'valid': valid,
                'errors': errors
            }

        except Exception as error:
            logging.error(error)
            pass
